﻿using TaskManager.Web.Api.Models;

namespace TaskManager.Web.Api.InquiryProcessor
{
    public interface ITaskByIdInquiryProcessor
    {
        Task GetTask(long taskId);
    }
}