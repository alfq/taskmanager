﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using TaskManager.Common.TypeMapping;

namespace TaskManager.Web.Api
{
    public class AutoMapperConfigurator
    {
        public void Configure(IEnumerable<IAutoMapperTypeConfigurator> autoMapperTypeConfigurators)
        {
            autoMapperTypeConfigurators.ToList().ForEach(x=>x.Configure());
            Mapper.AssertConfigurationIsValid();
        }
    }
}