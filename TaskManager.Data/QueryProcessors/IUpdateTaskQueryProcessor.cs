﻿using System.Collections.Generic;
using TaskManager.Data.Entities;
using PropertyValueMap = System.Collections.Generic.Dictionary<string, object>;

namespace TaskManager.Data.QueryProcessors
{
    public interface IUpdateTaskQueryProcessor
    {
        Task GetUpdatedTask(long taskId, PropertyValueMap updatedPropertyValueMap);
        Task ReplaceTaskUsers(long taskId, IEnumerable<long> userIds);
        Task DeleteTaskUsers(long taskId);
        Task AddTaskUser(long taskId, long userId);
        Task DeleteTaskUser(long taskId, long userId);
    }
}