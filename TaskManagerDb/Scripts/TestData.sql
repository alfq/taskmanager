﻿declare @statusId int,
		@taskId int,
		@userId int

if not exists (select * from [User] where Username = 'bjoe')
		INSERT into [dbo].[User] ([Firstname], [Lastname], [Username])
			VALUES (N'Billy', N'Joe',N'bjoe')

if not exists (select * from [User] where Username = 'jbosco')
		INSERT into [dbo].[User] ([Firstname], [Lastname], [Username])
			VALUES (N'John', N'Bosco', 'jbosco')

if not exists (select * from [User] where Username = 'jdoe')
		INSERT into [dbo].[User] ([Firstname], [Lastname], [Username])
			VALUES (N'James', N'Doe', 'jdoe')

if not exists (select * from dbo.Task where Subject = 'Test Task')
begin
		select top 1 @statusId = StatusId from Status order by StatusId;
		select top 1 @userId  = UserId from [User] order by UserId;

		insert into dbo.Task(Subject, StartDate, StatusId, CreatedDate, CreatedUserId)
				values ('Test Task', getDate(), @statusId, getDate(), @userId);

		set @taskId = SCOPE_IDENTITY();

		INSERT [dbo].[TaskUser] ([TaskId], [UserId])
				VALUES (@taskId, @userId)

end